export const getPagesForPagination = (dataLength, take, skip) => {
  const pageCount = Math.ceil(dataLength / take);
  const currentPage = skip / take + 1;

  if (pageCount < 6) return Array.from({ length: pageCount }, (_, i) => i + 1);
  const pages = new Set([1, pageCount, currentPage]);
  if (currentPage > 1) {
    pages.add(currentPage - 1);
  }

  if (currentPage < pageCount) {
    pages.add(currentPage + 1);
  }

  return [...pages].sort((a, b) => a - b).filter(page => page <= pageCount && page >= 1);
};
