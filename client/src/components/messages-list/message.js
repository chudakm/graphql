import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faThumbsDown, faThumbsUp } from "@fortawesome/free-regular-svg-icons";
import React, { useState } from "react";
import "./message-list.css";
import { Mutation } from "@apollo/client/react/components";
import { ADD_REPLY_MUTATION, DISLIKE_MESSAGE_MUTATION, LIKE_MESSAGE_MUTATION, MESSAGES_QUERY } from "../../graph-ql/queries";
import { deepClone } from "../../util/deep-clone";
import { PAGE_SIZE } from "./contants";

export const Message = ({ id, text, likes, dislikes, createdAt, replies, orderBy, filter, skip }) => {
  const [isShowingReplyForm, setIsShowingReplyForm] = useState(false);
  const [replyText, setReplyText] = useState("");

  const handleReplyClick = () => {
    setIsShowingReplyForm(!isShowingReplyForm);
  };

  const _updateStoreAfterReply = (store, newMessage) => {
    const { messages } = store.readQuery({ query: MESSAGES_QUERY, variables: { orderBy, filter, skip, first: PAGE_SIZE } });

    const messageList = deepClone(messages.messageList);

    const message = messageList.find(message => message.id === id);
    message.replies.push(newMessage);

    store.writeQuery({
      query: MESSAGES_QUERY,
      data: {
        messages: {
          messageList,
        },
      },
      variables: { orderBy, filter, skip, first: PAGE_SIZE },
    });
  };

  const _updateStoreAfterLikeMessage = (store, likes) => {
    const { messages } = store.readQuery({ query: MESSAGES_QUERY, variables: { orderBy, filter, skip, first: PAGE_SIZE } });
    const messageList = deepClone(messages.messageList);
    const message = messageList.find(message => message.id === id);
    message.likes = likes;

    store.writeQuery({
      query: MESSAGES_QUERY,
      data: {
        messages: {
          messageList,
        },
      },
      variables: { orderBy, filter, skip, first: PAGE_SIZE },
    });
  };

  const _updateStoreAfterDislikeMessage = (store, dislikes) => {
    const { messages } = store.readQuery({ query: MESSAGES_QUERY, variables: { orderBy, filter, skip, first: PAGE_SIZE } });
    const messageList = deepClone(messages.messageList);
    const message = messageList.find(message => message.id === id);
    message.dislikes = dislikes;

    store.writeQuery({
      query: MESSAGES_QUERY,
      data: {
        messages: {
          messageList,
        },
      },
      variables: { orderBy, filter, skip, first: PAGE_SIZE },
    });
  };

  const handleReplyComplete = () => {
    setIsShowingReplyForm(false);
    setReplyText("");
  };

  return (
    <div className='message'>
      <div className='message-header'>
        <span> {text} </span> <span> #{id}</span>
      </div>

      <div className='message-controls'>
        <div className='message-header-react'>
          <div className='react'>
            <Mutation
              mutation={LIKE_MESSAGE_MUTATION}
              variables={{ id }}
              update={(store, { data: { likeMessage } }) => {
                _updateStoreAfterLikeMessage(store, likeMessage);
              }}
            >
              {likeMessage => <FontAwesomeIcon icon={faThumbsUp} onClick={likeMessage} />}
            </Mutation>

            <span>{likes}</span>
          </div>

          <div className='react'>
            <Mutation
              mutation={DISLIKE_MESSAGE_MUTATION}
              variables={{ id }}
              update={(store, { data: { dislikeMessage } }) => {
                _updateStoreAfterDislikeMessage(store, dislikeMessage);
              }}
            >
              {dislikeMessage => <FontAwesomeIcon icon={faThumbsDown} onClick={dislikeMessage} />}
            </Mutation>
            <span>{dislikes}</span>
          </div>
        </div>

        <div>
          <button onClick={handleReplyClick}>{isShowingReplyForm ? "Cancel" : "Send"}</button>
        </div>
      </div>

      {isShowingReplyForm && (
        <div className='reply-form'>
          <input value={replyText} onInput={e => setReplyText(e.currentTarget.value)} />

          <Mutation
            mutation={ADD_REPLY_MUTATION}
            variables={{ messageId: id, text: replyText }}
            update={(store, { data: { addReply } }) => {
              _updateStoreAfterReply(store, addReply);
            }}
            onCompleted={handleReplyComplete}
          >
            {addReply => <button onClick={addReply}>Send</button>}
          </Mutation>
        </div>
      )}

      {replies.map(reply => {
        return <Reply key={reply.id} {...reply} orderBy={orderBy} filter={filter} skip={skip} />;
      })}
    </div>
  );
};

const Reply = ({ id, text, likes, dislikes, orderBy, filter, skip }) => {
  const _updateStoreAfterLikeReply = (store, likes) => {
    const { messages } = store.readQuery({ query: MESSAGES_QUERY, variables: { orderBy, filter, skip, first: PAGE_SIZE } });
    const messageList = deepClone(messages.messageList);
    const message = messageList.find(message => message.replies.findIndex(reply => reply.id === id) !== -1);
    const reply = message.replies.find(reply => reply.id === id);
    reply.likes = likes;

    store.writeQuery({
      query: MESSAGES_QUERY,
      data: {
        messages: {
          messageList,
        },
      },
      variables: { orderBy, filter, skip, first: PAGE_SIZE },
    });
  };

  const _updateStoreAfterDislikeReply = (store, dislikes) => {
    const { messages } = store.readQuery({ query: MESSAGES_QUERY, variables: { orderBy, filter, skip, first: PAGE_SIZE } });
    const messageList = deepClone(messages.messageList);
    const message = messageList.find(message => message.replies.findIndex(reply => reply.id === id) !== -1);
    const reply = message.replies.find(reply => reply.id === id);
    reply.dislikes = dislikes;

    store.writeQuery({
      query: MESSAGES_QUERY,
      data: {
        messages: {
          messageList,
        },
      },
      variables: { orderBy, filter, skip, first: PAGE_SIZE },
    });
  };

  return (
    <div className='reply'>
      <div className='reply-header'>
        <span>{text}</span> <span>#{id}</span>
      </div>

      <div className='reply-reactions'>
        <div className='react' style={{ marginRight: "2rem" }}>
          <Mutation
            mutation={LIKE_MESSAGE_MUTATION}
            variables={{ id }}
            update={(store, { data: { likeMessage } }) => {
              _updateStoreAfterLikeReply(store, likeMessage);
            }}
          >
            {likeMessage => <FontAwesomeIcon className='icon' icon={faThumbsUp} onClick={likeMessage} />}
          </Mutation>

          <span>{likes}</span>
        </div>

        <div className='react'>
          <Mutation
            mutation={DISLIKE_MESSAGE_MUTATION}
            variables={{ id }}
            update={(store, { data: { dislikeMessage } }) => {
              _updateStoreAfterDislikeReply(store, dislikeMessage);
            }}
          >
            {dislikeMessage => <FontAwesomeIcon className='icon' icon={faThumbsDown} onClick={dislikeMessage} />}
          </Mutation>

          <span>{dislikes}</span>
        </div>
      </div>
    </div>
  );
};
