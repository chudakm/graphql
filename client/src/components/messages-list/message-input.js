import { Mutation } from "@apollo/client/react/components";
import React, { useState } from "react";
import { CREATE_MESSAGE_MUTATION, MESSAGES_QUERY } from "../../graph-ql/queries";
import { PAGE_SIZE } from "./contants";
import "./message-list.css";

export const MessageInput = ({ orderBy, filter, skip }) => {
  const [text, setText] = useState("");

  const handleInput = e => {
    setText(e.currentTarget.value);
  };

  const _updateStoreAfterCreatingMessage = (store, newMessage) => {
    const data = store.readQuery({ query: MESSAGES_QUERY, variables: { orderBy, filter, skip, first: PAGE_SIZE } });

    store.writeQuery({
      query: MESSAGES_QUERY,
      data: {
        messages: {
          messageList: [newMessage, ...data.messages.messageList],
        },
      },
      variables: {
        orderBy,
        filter,
        skip,
        first: PAGE_SIZE,
      },
    });
  };

  return (
    <div className='message-input'>
      <textarea rows={10} cols={30} value={text} onInput={handleInput}></textarea>

      <Mutation
        mutation={CREATE_MESSAGE_MUTATION}
        variables={{ text }}
        update={(store, { data: { createMessage } }) => {
          _updateStoreAfterCreatingMessage(store, createMessage);
        }}
        onCompleted={() => setText("")}
      >
        {createMessage => (
          <button className='send-message' onClick={createMessage}>
            Send
          </button>
        )}
      </Mutation>
    </div>
  );
};
