import React, { useState } from "react";
import "./message-list.css";
import { Query } from "@apollo/client/react/components";
import { MESSAGES_QUERY, NEW_MESSAGE_SUBSCRIPTION } from "../../graph-ql/queries";
import { Message } from "./message";
import { MessageInput } from "./message-input";
import { getPagesForPagination } from "../../util/get-pages-for-pagination";
import { PAGE_SIZE } from "./contants";

const orderByOptionsMap = new Map();
orderByOptionsMap.set("createdAt DESC", "createdAt_DESC");
orderByOptionsMap.set("createdAt ASC", "createdAt_ASC");
orderByOptionsMap.set("likes DESC", "likes_DESC");
orderByOptionsMap.set("likes ASC", "likes_ASC");
orderByOptionsMap.set("dislikes DESC", "dislikes_DESC");
orderByOptionsMap.set("dislikes ASC", "dislikes_ASC");

const orderByOptionsMapKeys = [...orderByOptionsMap.keys()];

export const MessageList = () => {
  const [orderBy, setOrderBy] = useState("createdAt_DESC");
  const [filterText, setFilterText] = useState("");
  const [filter, setFilter] = useState("");
  const [skip, setSkip] = useState(0);

  const handleOrderBy = event => {
    setOrderBy(event.target.value);
  };

  const handleFilter = () => {
    setFilter(filterText);
  };

  const _subscribeToNewMessages = subscribeToMore => {
    subscribeToMore({
      document: NEW_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.messageList.find(({ id }) => id === newMessage.id);
        if (exists) return prev;

        return {
          ...prev,
          messages: {
            messageList: [newMessage, ...prev.messages.messageList],
            count: prev.messages.messageList.length + 1,
            __typename: prev.products.__typename,
          },
        };
      },
    });
  };

  return (
    <Query query={MESSAGES_QUERY} variables={{ orderBy, filter, skip, first: PAGE_SIZE }}>
      {({ loading, error, data, subscribeToMore }) => {
        if (loading) return <div className='message-list'>Loading...</div>;
        if (error) return <div className='message-list'>Fetch error</div>;
        _subscribeToNewMessages(subscribeToMore);

        const {
          messages: { count, messageList },
        } = data;

        return (
          <div className='message-list-container'>
            <div>
              <select size='1' value={orderBy} onChange={handleOrderBy}>
                {orderByOptionsMapKeys.map(key => {
                  return (
                    <option key={key} value={orderByOptionsMap.get(key)}>
                      {key}
                    </option>
                  );
                })}
              </select>

              <div className='filter'>
                <input value={filterText} onChange={e => setFilterText(e.currentTarget.value)} />
                <button onClick={handleFilter}>Filter</button>
              </div>

              <div className='pagination'>
                {getPagesForPagination(count, PAGE_SIZE, skip).map(page => {
                  return (
                    <button
                      className={`${(page - 1) * 3 === skip ? "active-page" : ""}`}
                      key={page}
                      onClick={() => setSkip((page - 1) * 3)}
                    >
                      {page}
                    </button>
                  );
                })}
              </div>
            </div>

            <div className='message-list'>
              {messageList.map(item => {
                return <Message key={item.id} {...item} orderBy={orderBy} filter={filter} skip={skip} />;
              })}
            </div>

            <MessageInput orderBy={orderBy} filter={filter} skip={skip} />
          </div>
        );
      }}
    </Query>
  );
};
