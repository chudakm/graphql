import "./App.css";
import { MessageList } from "./components/messages-list/message-list";

function App() {
  return (
    <div className='App'>
      <MessageList />
    </div>
  );
}

export default App;
