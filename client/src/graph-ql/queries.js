import { gql } from "@apollo/client";

export const MESSAGES_QUERY = gql`
  query messagesQuery($orderBy: MessagesOrderBy, $skip: Int, $first: Int, $filter: String) {
    messages(orderBy: $orderBy, skip: $skip, first: $first, filter: $filter) {
      count
      messageList {
        id
        text
        likes
        dislikes
        createdAt

        replies {
          id
          text
          likes
          dislikes
          createdAt
        }
      }
    }
  }
`;

export const CREATE_MESSAGE_MUTATION = gql`
  mutation createMessage($text: String!) {
    createMessage(text: $text) {
      id
      text
      likes
      dislikes
      createdAt

      replies {
        id
        text
        likes
        dislikes
        createdAt
      }
    }
  }
`;

export const ADD_REPLY_MUTATION = gql`
  mutation addReply($messageId: Int!, $text: String!) {
    addReply(messageId: $messageId, text: $text) {
      id
      text
      likes
      dislikes
      createdAt

      replies {
        id
        text
        likes
        dislikes
        createdAt
      }
    }
  }
`;

export const LIKE_MESSAGE_MUTATION = gql`
  mutation likeMessage($id: Int!) {
    likeMessage(id: $id)
  }
`;

export const DISLIKE_MESSAGE_MUTATION = gql`
  mutation dislikeMessage($id: Int!) {
    dislikeMessage(id: $id)
  }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription onNewMessage {
    newMessage {
      id
      text
      likes
      dislikes
      createdAt

      replies {
        id
        text
        likes
        dislikes
        createdAt
      }
    }
  }
`;
