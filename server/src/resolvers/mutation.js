const createMessage = async (parent, args, context, info) => {
  return context.prisma.createMessage({
    text: args.text,
    likes: 0,
    dislikes: 0,
  });
};

const addReply = async (parent, args, context, info) => {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId,
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.messageId} does not exist`);
  }

  const message = await context.prisma.createMessage({
    text: args.text,
    parent: {
      connect: { id: args.messageId },
    },
  });

  return message;
};

const likeMessage = async (parent, args, context, info) => {
  const message = await context.prisma.message({ id: args.id });
  const updatedMessage = await context.prisma.updateMessage({ data: { likes: message.likes + 1 }, where: { id: args.id } });
  return updatedMessage.likes;
};

const dislikeMessage = async (parent, args, context, info) => {
  const message = await context.prisma.message({ id: args.id });
  const updatedMessage = await context.prisma.updateMessage({ data: { dislikes: message.dislikes + 1 }, where: { id: args.id } });
  return updatedMessage.dislikes;
};

module.exports = {
  createMessage,
  addReply,
  likeMessage,
  dislikeMessage,
};
