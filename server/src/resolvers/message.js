const replies = async (parent, args, context) => {
  return context.prisma
    .message({
      id: parent.id,
    })
    .replies();
};

const parent = async (parent, args, context) => {
  return context.prisma.message({ id: parent.id }).parent();
};

module.exports = {
  replies,
  parent,
};
