function newMessageSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .message({
      mutation: ["CREATED"],
    })
    .node();
}

const newMessage = {
  subscribe: newMessageSubscribe,
  resolve: message => {
    return message;
  },
};

module.exports = {
  newMessage,
};
