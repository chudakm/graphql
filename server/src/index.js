const { GraphQLServer } = require("graphql-yoga");
const { prisma } = require("./generated/prisma-client");
const { GraphQLDateTime } = require("graphql-iso-date");
const Query = require("./resolvers/query");
const Mutation = require("./resolvers/mutation");
const Subscription = require("./resolvers/subscription");
const Message = require("./resolvers/message");

const resolvers = {
  Query,
  Mutation,
  Subscription,
  DateTime: GraphQLDateTime,
  Message,
};

const server = new GraphQLServer({
  typeDefs: "./src/schema.graphql",
  resolvers,
  context: { prisma },
});

server.start(() => console.log("http://localhost:4000"));
